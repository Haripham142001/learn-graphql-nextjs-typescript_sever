const TodoResolvers = require("./todo");
const UserResolvers = require("./user");

module.exports = {
  Query: {
    ...TodoResolvers.Query,
    ...UserResolvers.Query,
  },
  Mutation: {
    ...TodoResolvers.Mutation,
    ...UserResolvers.Mutation,
  },
};
