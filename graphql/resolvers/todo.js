const { v4: uuidv4 } = require("uuid");
const TodoModel = require("../../models/todo.model");

module.exports = {
  Mutation: {
    // async createUser(parent, args) {},
    async createTodo(parent, { userId, date, money, deadline, type, name }) {
      let newTodo,
        todo = {
          name,
          userId,
          date,
          type,
          sid: uuidv4(),
        };
      if (type === "task") {
        newTodo = new TodoModel({
          ...todo,
          deadline,
        });
      } else {
        newTodo = new TodoModel({
          ...todo,
          money,
        });
      }
      return await newTodo.save();
    },
  },
  Query: {
    // user: (parent, { sid }) => {},
    getAllTodo: async (_, { userId }) => {
      const listTodo = await TodoModel.find({ userId }).sort({ createdAt: -1 });
      return listTodo;
    },
    getMoney: async (_, { userId }) => {
      const listTodo = await TodoModel.find({ userId, type: "spend" });
      return listTodo;
    },
    getSumMoney: async (_, { userId }) => {
      try {
        const listTodo = await TodoModel.find({ userId, type: "spend" })
        const sum = listTodo.reduce((init, todo) => {
          return init + todo.money
        }, 0)
        return sum
      } catch (error) {
        console.log(error);
      }
    }
  },
};
