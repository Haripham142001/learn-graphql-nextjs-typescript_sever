const userModel = require("../../models/user.model");
const { ApolloError } = require("apollo-server-errors");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");

module.exports = {
  Mutation: {
    async register(parent, { registerInput: { email, password } }) {
      try {
        const oldUser = await userModel.findOne({ email });
        if (oldUser)
          throw new ApolloError("A user is already registered by this email");

        const encryptPassword = await bcrypt.hash(password, 10);
        const newUser = new userModel({
          email,
          password: encryptPassword,
          sid: uuidv4(),
        });

        const res = await newUser.save();

        return res;
      } catch (error) {
        console.log(error);
      }
    },
    async login(parent, { loginInput: { email, password } }) {
      try {
        const user = await userModel.findOne({ email });
        if (user && (await bcrypt.compare(password, user.password))) {
          const token = jwt.sign({ user_id: newUser.sid, email }, "HariPham", {
            expiresIn: "1d",
          });

          user.token = token;
          return user;
        } else {
          throw new ApolloError("Incorrect password", "INCORECT_PASSWORD");
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
  Query: {
    user: async (_, { sid }) => {
      try {
        const user = await userModel.findOne({ sid });
        return user || {};
      } catch (error) {
        console.log(error);
      }
    },
  },
};
