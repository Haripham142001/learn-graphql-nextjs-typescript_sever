const { gql } = require("apollo-server-express");

const typeDefs = gql`
  type User {
    id: ID
    sid: String
    password: String
    email: String
    image: String
    token: String
    status: String
  }

  type Todo {
    id: ID
    sid: String
    userId: String
    name: String
    type: String
    date: String
    deadline: String
    money: Int
  }

  input RegisterInput {
    email: String
    password: String
  }

  input LoginInput {
    email: String
    password: String
  }

  type Query {
    user(sid: String!): User
    getAllTodo(userId: String!): [Todo]
    getMoney(userId: String!): [Todo]
    getSumMoney(userId: String!): Int
  }

  type Mutation {
    register(registerInput: RegisterInput): User
    login(loginInput: LoginInput): User
    createTodo(
      userId: String!
      date: String!
      name: String!
      type: String!
      money: Int
      deadline: String
    ): Todo
  }
`;

module.exports = typeDefs;
