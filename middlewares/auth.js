const { AuthenticationError } = require("apollo-server-express");

const jwt = require("jsonwebtoken");

module.exports = ({ req }, next) => {
  const authHeader = req.headers.authorization;
  console.log(req);
  if (authHeader) {
    const token = authHeader.split("Bearer")[1].trim();
    if (token) {
      try {
        //verify token
        const user = {hello: "a"}
        return user
      } catch (error) {
        throw new AuthenticationError("Invalid/Expired token");
      }
    }
    throw new Error("Authentication token must be 'Bearer [token]'");
  }
  throw new Error("Authentication token must be provided");
};
